"""
    This module creates all tables (and drops them before).
"""

import sys
import mariadb

# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user="solar",
        password="solar69fetch",
        host="172.24.228.83",
        port=3306,
        database="solarfetch"

    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor()

try:
    cur.execute(
        "DROP TABLE IF EXISTS InverterData;")
except mariadb.Error as e:
    print(f"Error dropping table InverterData: {e}")
    sys.exit(1)

try:
    cur.execute(
        """CREATE TABLE InverterData(
            data_id INT auto_increment NOT NULL,
            time_stamp_UTC DATETIME NOT NULL,
            power_W INT NOT NULL,
            CONSTRAINT InverterData_PK PRIMARY KEY (data_id)
            );""")
except mariadb.Error as e:
    print(f"Error creating table InverterData: {e}")
    sys.exit(1)

conn.commit()

# Close Connection
conn.close()
