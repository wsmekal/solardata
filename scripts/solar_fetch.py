"""
    This module should be used in a cron job. It requests data from the
    Fronius converter and pushes the relevant data to the mariadb database.
"""

import sys
import json
import requests
import mariadb
import dateutil.parser

# configuration
IP = "192.168.178.147"
URL = ( "http://" + IP + "/solar_api/v1/GetInverterRealtimeData.cgi?"
        "Scope=Device&DataCollection=CumulationInverterData" )

# get data from Fronius converter
# exit if request was not successful (most likely timeout)
try:
    response = requests.get(URL, timeout=2)
except requests.exceptions.RequestException as requests_except:
    raise SystemExit(requests_except) from requests_except

# print(response.content.decode("utf-8"))

data = json.loads(response.text)

ts = data["Head"]["Timestamp"] # 2021-10-18T05:42:58+00:00
power = round(data["Body"]["Data"]["PAC"]["Value"])

time_stamp = dateutil.parser.isoparse(ts)

# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user="solar",
        password="solar69fetch",
        host="172.24.228.83",
        port=3306,
        database="solarfetch"

    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor()

try:
    print(time_stamp, power)
    cur.execute(
        "INSERT INTO InverterData (time_stamp_UTC,power_W) VALUES (?, ?)",
    (time_stamp, power))
    conn.commit()
except mariadb.Error as e:
    print(f"Error inserting data: {e}")
    sys.exit(1)

# Close Connection
conn.close()
