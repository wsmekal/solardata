# LEA SCS

Ablaufsteuerung (sequence control system - SCS) für die Porsche Demozelle. Umfasst Anbindung an SPS und Roboter-SPS über Profinet,
Linear stages und PicoScope Device. C++ Programm, verwendet wxWidgets, LUA, u.a.

# Prerequisites

## Install Visual C++ 2019 Community Edition

* Download here: [Visual Studio 2019 für Windows und Mac herunterladen](https://visualstudio.microsoft.com/de/downloads/)
* On page "Workloads" select "Desktopentwicklung mit C++""
* Select Details for installation on the right:
  * MSVC v142 - VS 2019 ...
  * Windows 10 SDK (latest version)
  * Just-in_time-Debugger
  * C++-Profilererstellungstool
  * C++-CMake-Tools für Windows
  * C++-ATL für die neuesten v142-Buildtools
  * C++ AddressSanitizer
  * Download and Install
  * Likely Restart

## Install InnoSetup

* Download here: [Inno Setup Downloads](https://jrsoftware.org/isdl.php)
* Download the Stable Release (6.1.2 at the time of writing)

## Install git for Windows

* Download here: https://gitforwindows.org/
* Install - Important options: "Use OpenSSH", "Use the OpenSSL library", "Checkout Windows-style", "Use MinTTY" (stay otherwise with recommended options)
* We follow the "gitflow"-Git branching model described here: https://nvie.com/posts/a-successful-git-branching-model/

## Setup command line environment

* Create directory C:\Development
* I like to use cmder - so you like it too!
* Downloader cmder Mini from here https://cmder.net/
* Unzip package to C:\Development\cmder
* Create a shortlink to cmder.exe on the Desktop - right click on properties of shortcut and change "Ausführen in" to C:\Development
* Create with vim a file called profile.bat. Enter:
  * @echo off
  * REM Setup wxWidgets
  * set WXWIN=C:\Development\wxWidgets-3.1.5
* Before running cmake in the lea-scs directory, run the profile.bat batch file

## Install wxWidgets library

* Download and unzip wxWidgets 3.1.5 source code to C:\Development\wxWidgets-3.1.5 from here: http://wxwidgets.org/downloads/  
* Open wx_vc16.sln in build/msw in Visual Studio 2019 (open it a second time, if first time error occurs)
* Set "Debug" and "x64" and build with "Projektmappe erstellen (F7)"
* Set "Release" and "x64" and build with "Projektmappe erstellen (F7)""
* Test if all is okay
  * Open "minimal_vc15.sln" in samples/minimal
  * Set "Debug" and "x64" and build with "Projektmappe erstellen (F7)"
  * Run debug with F5

## Install cmake

* Download cmake win64-x64 Installer (3.17.3 at the time of writing) frome [Download | CMake](https://cmake.org/download/)
* Install - choose option "Add CMake to the system PATH for all users"

## Install Dialogblocks

* Download Version 5.16 standalone distribution from here http://dialogblocks.com/download.htm
* Install it
* Registration information: Name: Werner Smekal, Key: FCAB138D-FA241E3B-20F62FDA

## PicoScope SDK

* Download SDK from https://www.picotech.com/downloads (PicoSDK 10.7.21 207(64bit) at the time of writing)
* Install SDK. Choose the following for install:
  * Kernel Drivers - System Support
  * PicoScope Devices - ps4000/ps4000a/ps5000/ps5000a (at least)
  * No PicoLog Devices
* This will also install the needed drivers for the used PicoScopes.

# Obtain source code

* Start Cmder terminal
* Go to C:\Development
* Follow these instructions http://gitlab.xarion.com/help/ssh/README#generating-a-new-ssh-key-pair to create a ssh key and store it in the settings.
* Clone repository: git clone ssh://git@gitlab.xarion.com:222/application/lea-scs.git
* Don#t forget to checkout the branch you want to work on, e.g. git checkout develop

# Compilation

* start cmd.exe or Cmder terminal
* setup environment
  * cd \Development
  * profile.bat
* Enter project directory and create build directory
  * cd lea-scs
  * mkdir visualc
  * cd visualc
* create Visual C++ Project File
  * cmake -G "Visual Studio 16 2019" -A x64 ..
  * open lea_scs.sln
* build "lea_scs"

# Create installer

* Delete visualc folder and follow "Compilation" section above.
* Create a release version (x64)
* Edit the file installer/lea-scs.iss
* Change the line "#define REVISION 0" accordingly (usually add 1)
* Compile the installer with "Inno Setup Compiler" or "Inno Script Studio"
* Installer will then be in installer\Output

# Miscellaneous

## Binary file format

Information of a single measurement contains

* scan position in encoder ticks (uint16 x *1*)
* step position in encoder ticks (uint16 x *1*)
* ADC values, arbitrary dimension (int16 x *Samples*) 

The binary file contains *n* measurements. The binary file format is defined as:

**Header (once)**

* Number of measurements *n* (uint16 x *1*)
* Number of *samples* per measurement (uint16 x *1*)
* Encoder ticks per mm for scan axis (uint16 x *1*)
* Encoder ticks per mm for step axis (uint16 x *1*)
* ADC maximal range in mV (uint16 x *1*)

**Body (*n* times)**

* scan position in encoder ticks (uint16 x *1*)
* step position in encoder ticks (uint16 x *1*)
* ADC values, arbitrary dimension (int16 x *Samples*)

## Change crontab
* crontab -e
  * * * * * * cd ~/SolarFetch && . .venv/bin/activate && python solar_fetch.py > /tmp/out1 2>&1

## Software & Resources

* https://www.hw-group.com/software/hercules-setup-utility - serial port/UDP/TCP terminal
* http://sockettest.sourceforge.net - tool for socket testing
* https://www.proxoft.com/BinaryViewer.aspx - binary file viewer
* http://www.gentleface.com/free_icon_set.html - icons
